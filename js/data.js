var data = [
 {
   "STT": 1,
   "Name": "Bệnh viện An Bình",
   "address": "146 An Bình,Phường 7, Quận 5,Hồ Chí Minh",
   "Longtitude": 10.754244,
   "Latitude": 106.671571
 },
 {
   "STT": 2,
   "Name": "Bệnh viện Cấp cứu Trưng Vương",
   "address": "266 Lý Thường Kiệt,Phường 14, Quận 10,Hồ Chí Minh",
   "Longtitude": 10.770735,
   "Latitude": 106.659406
 },
 {
   "STT": 3,
   "Name": "Bệnh viện Đa khoa khu vực Củ Chi",
   "address": "ấp Bầu Tre 2,Xã Tân An Hội,Huyện Củ Chi,Hồ Chí Minh",
   "Longtitude": 10.975661,
   "Latitude": 106.477031
 },
 {
   "STT": 4,
   "Name": "Bệnh viện Đa khoa khu vực Hóc Môn",
   "address": "65/2B Bà Triệu,Thị trấn Hóc Môn,Huyện Hóc Môn,Hồ Chí Minh",
   "Longtitude": 10.882583,
   "Latitude": 106.593398
 },
 {
   "STT": 5,
   "Name": "Bệnh viện Đa khoa khu vực Thủ Đức",
   "address": "64 Lê Văn Chí,Phường Linh Trung,Quận Thủ Đức,Hồ Chí Minh",
   "Longtitude": 10.861541,
   "Latitude": 106.780451
 },
 {
   "STT": 6,
   "Name": "Bệnh viện Đa khoa Sài Gòn",
   "address": "125 Lê Lợi,Phường Bến Thành,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.771984,
   "Latitude": 106.699259
 },
 {
   "STT": 7,
   "Name": "Bệnh viện Nhân dân 115",
   "address": "88 Thành Thái,Phường 12,Quận 10,Hồ Chí Minh",
   "Longtitude": 10.774428,
   "Latitude": 106.666282
 },
 {
   "STT": 8,
   "Name": "Bệnh viện Nhân dân Gia Định",
   "address": "1 Nơ Trang Long,Phường 7,Quận Bình Thạnh,Hồ Chí Minh",
   "Longtitude": 10.80378,
   "Latitude": 106.694184
 },
 {
   "STT": 9,
   "Name": "Bệnh viện Nguyễn Trãi",
   "address": "314 Nguyễn Trãi,Phường 8,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.756781,
   "Latitude": 106.675077
 },
 {
   "STT": 10,
   "Name": "Bệnh viện Nguyễn Tri Phương",
   "address": "468 Nguyễn Trãi,Phường 8,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.75559,
   "Latitude": 106.670178
 },
 {
   "STT": 11,
   "Name": "Bệnh viện Bệnh Nhiệt Đới",
   "address": "190 Bến Hàm Tử \n(764 Võ Văn Kiệt),Phường 1,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.752487,
   "Latitude": 106.678898
 },
 {
   "STT": 12,
   "Name": "Bệnh viện Bình Dân",
   "address": "371 Điện Biên Phủ,Phường 4,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.775118,
   "Latitude": 106.681366
 },
 {
   "STT": 13,
   "Name": "Bệnh viện Chấn thương Chỉnh hình",
   "address": "929 Trần Hưng Đạo,Phường 1,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.75418,
   "Latitude": 106.678235
 },
 {
   "STT": 14,
   "Name": "Bệnh viện Da Liễu",
   "address": "2 Nguyễn Thông,Phường 6,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.776801,
   "Latitude": 106.686948
 },
 {
   "STT": 15,
   "Name": "Bệnh viện Phục hồi chức năng và Điều trị bệnh nghề nghiệp",
   "address": "125/61 Âu Dương Lân,Phường 2,Quận 8,Hồ Chí Minh",
   "Longtitude": 10.741307,
   "Latitude": 106.686281
 },
 {
   "STT": 16,
   "Name": "Bệnh viện Hùng Vương",
   "address": "128 Hồng Bàng ,Phường 12,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.755856,
   "Latitude": 106.661781
 },
 {
   "STT": 17,
   "Name": "Bệnh viện Mắt",
   "address": "280 Điện Biên Phủ,Phường 7,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.778551,
   "Latitude": 106.684936
 },
 {
   "STT": 19,
   "Name": "Bệnh viện Nhi Đồng 1",
   "address": "341 Sư Vạn Hạnh,Phường 10,Quận 10,Hồ Chí Minh",
   "Longtitude": 10.76856,
   "Latitude": 106.670159
 },
 {
   "STT": 20,
   "Name": "Bệnh viện Nhi Đồng 2",
   "address": "14 Lý Tự Trọng,Phường Bến Nghé,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.781524,
   "Latitude": 106.702639
 },
 {
   "STT": 21,
   "Name": "Bệnh viện Nhi Đồng thành phố",
   "address": ",Xã Tân Kiên,Huyện Bình Chánh,Hồ Chí Minh",
   "Longtitude": 10.710207,
   "Latitude": 106.574302
 },
 {
   "STT": 22,
   "Name": "Bệnh viện Phạm Ngọc Thạch",
   "address": "120 Hồng Bàng,Phường 12,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.757208,
   "Latitude": 106.665105
 },
 {
   "STT": 23,
   "Name": "Bệnh viện Răng Hàm Mặt",
   "address": "263-265 Trần Hưng Đạo,Phường Cô Giang,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.763592,
   "Latitude": 106.691842
 },
 {
   "STT": 24,
   "Name": "Bệnh viện Tâm Thần",
   "address": "766 Võ Văn Kiệt ,Phường 1,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.752401,
   "Latitude": 106.678347
 },
 {
   "STT": 25,
   "Name": "Bệnh viện Tai Mũi Họng",
   "address": "153-155B Trần Quốc Thảo,Phường 9,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.784371,
   "Latitude": 106.684046
 },
 {
   "STT": 26,
   "Name": "Bệnh viện Từ Dũ",
   "address": "284 Cống Quỳnh,Phường Phạm Ngũ Lão,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.768799,
   "Latitude": 106.685821
 },
 {
   "STT": 27,
   "Name": "Bệnh viện Truyền máu Huyết học",
   "address": "118 Hùng Vương,Phường 12,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.756406,
   "Latitude": 106.665882
 },
 {
   "STT": 28,
   "Name": "Bệnh viện Ung Bướu",
   "address": "3 Nơ Trang Long,Phường 7,Quận Bình Thạnh,Hồ Chí Minh",
   "Longtitude": 10.805323,
   "Latitude": 106.694953
 },
 {
   "STT": 29,
   "Name": "Bệnh viện Y học Cổ truyền",
   "address": "179 Nam Kỳ Khởi Nghĩa,Phường 7,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.785959,
   "Latitude": 106.687702
 },
 {
   "STT": 30,
   "Name": "BỆNH VIỆN ĐẠI HỌC Y HÀ NỘI",
   "address": "338 Hai Bà Trưng,Phường Tân Định,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.79003,
   "Latitude": 106.689525
 },
 {
   "STT": 31,
   "Name": "Bệnh viện Quận 2",
   "address": "130 Lê Văn Thịnh,Phường Bình Trưng Tây,Quận 2,Hồ Chí Minh",
   "Longtitude": 10.782347,
   "Latitude": 106.768469
 },
 {
   "STT": 32,
   "Name": "Bệnh viện Quận 3",
   "address": "114-116 Trần Quốc Thảo,Phường 7,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.784371,
   "Latitude": 106.684046
 },
 {
   "STT": 33,
   "Name": "Bệnh viện Quận 4",
   "address": "63-65 Bến Vân Đồn,Phường 12,Quận 4,Hồ Chí Minh",
   "Longtitude": 10.765305,
   "Latitude": 106.70225
 },
 {
   "STT": 34,
   "Name": "Bệnh viện Quận 5",
   "address": "644 Nguyễn Trãi,Phường 11,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.75395,
   "Latitude": 106.666066
 },
 {
   "STT": 35,
   "Name": "Bệnh viện Quận 6",
   "address": "2D đường Chợ Lớn,Phường 11,Quận 6,Hồ Chí Minh",
   "Longtitude": 10.74656,
   "Latitude": 106.634523
 },
 {
   "STT": 36,
   "Name": "Bệnh viện Quận 7",
   "address": "101 Nguyễn Thị Thập,Phường Tân Phú,Quận 7,Hồ Chí Minh",
   "Longtitude": 10.737319,
   "Latitude": 106.724189
 },
 {
   "STT": 37,
   "Name": "Bệnh viện Quận 8",
   "address": "82 Cao Lỗ,Phường 4,Quận 8,Hồ Chí Minh",
   "Longtitude": 10.741574,
   "Latitude": 106.67641
 },
 {
   "STT": 38,
   "Name": "Bệnh viện Quận 9",
   "address": "Khu Phố 2 Lê Văn Việt,Phường Tăng Nhơn Phú A,Quận 9,Hồ Chí Minh",
   "Longtitude": 10.844897,
   "Latitude": 106.790257
 },
 {
   "STT": 39,
   "Name": "BỆNH VIỆN ĐẠI HỌC Y HÀ NỘI0",
   "address": "571 Sư Vạn Hạnh (nối dài),Phường 13,Quận 10,Hồ Chí Minh",
   "Longtitude": 10.776175,
   "Latitude": 106.66657
 },
 {
   "STT": 40,
   "Name": "BỆNH VIỆN ĐẠI HỌC Y HÀ NỘI1",
   "address": "72 Đường số 5, Cư Xá Bình Thới,Phường 8,Quận 11,Hồ Chí Minh",
   "Longtitude": 10.760772,
   "Latitude": 106.647891
 },
 {
   "STT": 41,
   "Name": "BỆNH VIỆN ĐẠI HỌC Y HÀ NỘI2",
   "address": "111 TCH 21,Phường Tân Chánh Hiệp,Quận 12,Hồ Chí Minh",
   "Longtitude": 10.86072,
   "Latitude": 106.630515
 },
 {
   "STT": 42,
   "Name": "Bệnh viện Đa khoa huyện Bình Chánh",
   "address": "E9/5 Nguyễn Hữu TríK,Phường 5 Thị trấn Tân Túc,Huyện Bình Chánh,Hồ Chí Minh",
   "Longtitude": 10.696489,
   "Latitude": 106.593969
 },
 {
   "STT": 43,
   "Name": "Bệnh viện Quận Bình Tân",
   "address": "809 Hương Lộ 2,Phường Bình Trị Đông A,Quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.764652,
   "Latitude": 106.603513
 },
 {
   "STT": 44,
   "Name": "Bệnh viện Quận Bình Thạnh",
   "address": "112AB Đinh Tiên Hoàng,Phường 1,Quận Bình Thạnh,Hồ Chí Minh",
   "Longtitude": 10.79802,
   "Latitude": 106.696662
 },
 {
   "STT": 45,
   "Name": "Bệnh viện Đa khoa huyện Cần Giờ",
   "address": "KP Miễu 3,Thị trấn Cần Thạnh,Huyện Cần Giờ,Hồ Chí Minh",
   "Longtitude": 10.407123,
   "Latitude": 106.965136
 },
 {
   "STT": 46,
   "Name": "Bệnh viện Đa khoa huyện Củ Chi",
   "address": "258 Tỉnh lộ 7Ấp Chợ cũ 2,Xã An Nhơn Tây,Huyện Củ Chi,Hồ Chí Minh",
   "Longtitude": 11.084952,
   "Latitude": 106.509724
 },
 {
   "STT": 47,
   "Name": "Bệnh viện Hóc Môn",
   "address": "65/2B Bà Triệu, Thị trấn Hóc Môn, Hóc Môn, Hồ Chí Minh",
   "Longtitude": 10.85581,
   "Latitude": 106.607313
 },
 {
   "STT": 48,
   "Name": "Bệnh viện Quận Gò Vấp",
   "address": "212 Lê Đức Thọ ( 26/3 ),Phường 15,Quận Gò Vấp,Hồ Chí Minh",
   "Longtitude": 10.834952,
   "Latitude": 106.661608
 },
 {
   "STT": 49,
   "Name": "Bệnh viện Đa khoa huyện Nhà Bè",
   "address": "281A Lê Văn LươngẤp 3,Xã Phước Kiển,Huyện Nhà Bè,Hồ Chí Minh",
   "Longtitude": 10.704446,
   "Latitude": 106.704459
 },
 {
   "STT": 50,
   "Name": "Bệnh viện Quận Phú Nhuận",
   "address": "274 Nguyễn Trọng Tuyển,Phường 8,Quận Phú Nhuận,Hồ Chí Minh",
   "Longtitude": 10.798203,
   "Latitude": 106.671828
 },
 {
   "STT": 51,
   "Name": "Bệnh viện Quận Tân Bình",
   "address": "605 Hoàng Văn Thụ,Phường 4,Quận Tân Bình,Hồ Chí Minh",
   "Longtitude": 10.79447,
   "Latitude": 106.655339
 },
 {
   "STT": 52,
   "Name": "BỆNH VIỆN ĐẠI HỌC Y HÀ NỘI",
   "address": "609-611 Âu Cơ,Phường Phú Trung,Quận Tân Phú,Hồ Chí Minh",
   "Longtitude": 10.783553,
   "Latitude": 106.642027
 },
 {
   "STT": 53,
   "Name": "Bệnh viện Quận Thủ Đức",
   "address": "29 Phú ChâuKP 5,Phường Tam Phú,Quận Thủ Đức,Hồ Chí Minh",
   "Longtitude": 10.864326,
   "Latitude": 106.745576
 },
 {
   "STT": 54,
   "Name": "Bệnh viện Chợ Rẫy",
   "address": "201B Nguyễn Chí Thanh, Phường 12, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7570385,
   "Latitude": 106.6575418
 },
 {
   "STT": 55,
   "Name": "Bệnh viện Thống Nhất ",
   "address": "1 Lý Thường Kiệt, Phường 7, Tân Bình, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7913464,
   "Latitude": 106.6512435
 },
 {
   "STT": 56,
   "Name": "Bệnh viện Răng - Hàm - Mặt Trung ương thành phố Hồ Chí Minh",
   "address": "201A Nguyễn Chí Thanh, Phường 12, Quận 5, Thành phố Hồ Chí Minh",
   "Longtitude": 10.7578734,
   "Latitude": 106.6583243
 },
 {
   "STT": 65,
   "Name": "Bệnh viện Quốc Ánh",
   "address": "110 đường số 54, Khu Tân Tạo Phường Tân Tạo Quận Bình Tân",
   "Longtitude": 10.7536074,
   "Latitude": 106.593305
 },
 {
   "STT": 66,
   "Name": "Bệnh viện Quốc tế Thành Đô",
   "address": "3 Đường Số 17A, Bình Trị Đông B, Bình Tân, Hồ Chí Minh",
   "Longtitude": 10.741209,
   "Latitude": 106.609158
 },
 {
   "STT": 67,
   "Name": "Bệnh viện Chuyên khoa Ngoại Minh Anh",
   "address": "36 Đường 1B Phường Bình Trị Đông Quận Bình Tân",
   "Longtitude": 10.7476736,
   "Latitude": 106.6144635
 },
 {
   "STT": 68,
   "Name": "Bệnh viện Triều An",
   "address": "425 Kinh Dương Vương Phường An Lạc Quận Bình Tân",
   "Longtitude": 10.7392829,
   "Latitude": 106.6171191
 },
 {
   "STT": 69,
   "Name": "Bệnh viện Quốc tế Columbia Asia Gia Định",
   "address": "1 Nơ Trang Long Phường 7 Quận Bình Thạnh",
   "Longtitude": 10.8037822,
   "Latitude": 106.6941877
 },
 {
   "STT": 70,
   "Name": "Bệnh viện Đa khoa Quốc tế Vinmec Central Park ",
   "address": "208 Nguyễn Hữu Cảnh, Phường 22, Bình Thạnh, Hồ Chí Minh",
   "Longtitude": 10.794536,
   "Latitude": 106.720209
 },
 {
   "STT": 71,
   "Name": "Bệnh viện Xuyên Á",
   "address": "Quốc lộ 22, Ấp Chợ Xã Tân Phú Trung Huyện Củ Chi",
   "Longtitude": 10.928124,
   "Latitude": 106.55827
 },
 {
   "STT": 72,
   "Name": "Bệnh viện Hồng Đức III",
   "address": "Số 3/2 Thống Nhất Phường 10 Quận Gò Vấp",
   "Longtitude": 10.832949,
   "Latitude": 106.664578
 },
 {
   "STT": 73,
   "Name": "Bệnh viện Vũ Anh",
   "address": "15-16 Phan Văn Trị Phường 7 Quận Gò Vấp",
   "Longtitude": 10.828781,
   "Latitude": 106.68559
 },
 {
   "STT": 74,
   "Name": "Bệnh viện An Sinh",
   "address": "10 Trần Huy Liệu Phường 12 Quận Phú Nhuận",
   "Longtitude": 10.7910586,
   "Latitude": 106.6786412
 },
 {
   "STT": 75,
   "Name": "Bệnh viện Đa khoa Fortis - Hoàn Mỹ Sài Gòn",
   "address": "60-60A Phan Xích Long Phường 1 Quận Phú Nhuận",
   "Longtitude": 10.800102,
   "Latitude": 106.684257
 },
 {
   "STT": 76,
   "Name": "Bệnh viện Ngọc Linh",
   "address": "43R/2-R/4  Hồ Văn Huê Phường 9 Quận Phú Nhuận",
   "Longtitude": 10.802253,
   "Latitude": 106.676205
 },
 {
   "STT": 77,
   "Name": "Bệnh viện Sài Gòn – ITO Phú Nhuận",
   "address": "140C Nguyễn Trọng Tuyển Phường 8 Quận Phú Nhuận",
   "Longtitude": 10.7979685,
   "Latitude": 106.6767987
 },
 {
   "STT": 78,
   "Name": "Bệnh viện Chấn thương chỉnh hình Sài Gòn - ITO",
   "address": "305 Lê Văn Sỹ Phường 1 Quận Tân Bình",
   "Longtitude": 10.7947606,
   "Latitude": 106.6670762
 },
 {
   "STT": 79,
   "Name": "Bệnh viện Phụ Sản Mekong",
   "address": "243 - 243A - 243B Hoàng Văn Thụ Phường 1 Quận Tân Bình",
   "Longtitude": 10.799959,
   "Latitude": 106.667737
 },
 {
   "STT": 80,
   "Name": "Bệnh viện Tân Sơn Nhất",
   "address": "2B Phổ Quang Phường 2 Quận Tân Bình",
   "Longtitude": 10.802961,
   "Latitude": 106.667266
 },
 {
   "STT": 81,
   "Name": "Bệnh viện Gaya Việt Hàn",
   "address": "23-25A1 Trương Công Định Phường 14 Quận Tân Bình",
   "Longtitude": 10.7958934,
   "Latitude": 106.6410338
 },
 {
   "STT": 82,
   "Name": "Bệnh viện Mỹ Đức",
   "address": "4  Núi Thành Phường 13 Quận Tân Bình",
   "Longtitude": 10.7998548,
   "Latitude": 106.6417611
 },
 {
   "STT": 83,
   "Name": "Bệnh viện Ngoại thần Kinh Quốc tế",
   "address": "65A Lũy Bán Bích Phường Tân Thới Hòa Quận Tân Phú",
   "Longtitude": 10.7612349,
   "Latitude": 106.632077
 },
 {
   "STT": 84,
   "Name": "Bệnh viện Đa khoa Tâm Trí Sài Gòn",
   "address": "171/3 Trường Chinh Phường Tân Thới Nhất Quận 12",
   "Longtitude": 10.8317366,
   "Latitude": 106.6218418
 },
 {
   "STT": 85,
   "Name": "Bệnh viện Giải phẩu thẩm mỹ Sài Gòn",
   "address": "97B-99 Nguyễn Du Phường Bến Thành Quận 1",
   "Longtitude": 10.7750494,
   "Latitude": 106.6968165
 },
 {
   "STT": 86,
   "Name": "Bệnh viện Giải phẩu thẩm mỹ Thanh Vân",
   "address": "33G-H Nguyễn Bỉnh Khiêm  Phường Đa Kao Quận 1",
   "Longtitude": 10.789595,
   "Latitude": 106.702245
 },
 {
   "STT": 87,
   "Name": "Bệnh viện Mắt Sài Gòn II",
   "address": "100 Lê Thị Riêng Phường Bến Thành Quận 1",
   "Longtitude": 10.7714086,
   "Latitude": 106.690894
 },
 {
   "STT": 88,
   "Name": "Bệnh viện Phẫu thuật tạo hình thẩm mỹ Á - Âu",
   "address": "32C - D Thủ Khoa Huân Phường Bến Thành Quận 1",
   "Longtitude": 10.7738146,
   "Latitude": 106.6973274
 },
 {
   "STT": 89,
   "Name": "Bệnh viện Phẩu thuật thẩm mỹ Hàn Quốc Kim Hospital ",
   "address": "31 Nguyễn Đình Chiểu Phường Đa Kao Quận 1",
   "Longtitude": 10.786358,
   "Latitude": 106.697643
 },
 {
   "STT": 90,
   "Name": "Bệnh viện Phụ Sản Quốc Tế",
   "address": "69 Bùi Thị Xuân Phường Phạm Ngũ Lão Quận 1",
   "Longtitude": 10.7696087,
   "Latitude": 106.6884295
 },
 {
   "STT": 91,
   "Name": "Bệnh viện Tai Mũi Họng Sài Gòn",
   "address": "1-3 Trịnh Văn Cấn Phường Cầu Ông Lãnh Quận 1",
   "Longtitude": 10.7673182,
   "Latitude": 106.6968133
 },
 {
   "STT": 92,
   "Name": "Bệnh viện thẩm mỹ Hàn Quốc JW",
   "address": "44-46-48-50 Tôn Thất Tùng Phường Bến Thành Quận 1",
   "Longtitude": 10.7696354,
   "Latitude": 106.6891738
 },
 {
   "STT": 93,
   "Name": "Bệnh viện Thẩm mỹ Việt - Mỹ",
   "address": "331 Nguyễn Trãi  Phường Nguyễn Cư Trinh Quận 1",
   "Longtitude": 10.7619683,
   "Latitude": 106.686587
 },
 {
   "STT": 94,
   "Name": "Bệnh viện Đa khoa Mắt Sài Gòn",
   "address": "471/2 - 473 Cách Mạng Tháng 8 Phường 13 Quận 10",
   "Longtitude": 10.781837,
   "Latitude": 106.6739112
 },
 {
   "STT": 95,
   "Name": "Bệnh viện Mắt Việt - Hàn",
   "address": "355-365 Ngô Gia Tự Phường 3 Quận 10",
   "Longtitude": 10.761382,
   "Latitude": 106.670256
 },
 {
   "STT": 96,
   "Name": "Bệnh viện Ngoại khoa và Chấn thương chỉnh hình Phương Đông",
   "address": "79 Thành Thái Phường 14 Quận 10",
   "Longtitude": 10.7732935,
   "Latitude": 106.6647684
 },
 {
   "STT": 97,
   "Name": "Bệnh viện Vạn Hạnh",
   "address": "72-74 Sư Vạn Hạnh Phường 12 Quận 10",
   "Longtitude": 10.7713451,
   "Latitude": 106.6701416
 },
 {
   "STT": 98,
   "Name": "Bệnh viện Mắt kỹ thuật cao Phương Nam",
   "address": "360 Điện Biên Phủ Phường 11 Quận 10",
   "Longtitude": 10.7744297,
   "Latitude": 106.6808515
 },
 {
   "STT": 99,
   "Name": "Bệnh viện Thẩm mỹ EMCAS",
   "address": "291 (Cổng số 3 - Hẻm 285) Cách Mạng Tháng Tám Phường 12 Quận 10",
   "Longtitude": 10.780096,
   "Latitude": 106.676012
 },
 {
   "STT": 100,
   "Name": "Bệnh viện Thẩm mỹ Kỳ Hòa-Medika",
   "address": "262 đường 3 Tháng 2 Phường 12 Quận 10",
   "Longtitude": 10.7706061,
   "Latitude": 106.6720959
 },
 {
   "STT": 101,
   "Name": "Bệnh viện Quốc tế Phúc An Khang",
   "address": "800 Đồng Văn Cống, Khu phố 1 Phường Thạnh Mỹ Lợi Quận 2",
   "Longtitude": 10.7801626,
   "Latitude": 106.7563211
 },
 {
   "STT": 102,
   "Name": "Bệnh viện CKPT thẩm mỹ quốc tế Thảo Điền",
   "address": "95/3 Thảo Điền Phường Thảo Điền Quận 2",
   "Longtitude": 10.8073544,
   "Latitude": 106.7333595
 },
 {
   "STT": 103,
   "Name": "Bệnh viện Phẫu thuật tạo hình thẩm mỹ AVA Văn Lang",
   "address": "236-238 Võ Văn Tần Phường T5 Quận 3",
   "Longtitude": 10.7727349,
   "Latitude": 106.6863611
 },
 {
   "STT": 104,
   "Name": "Bệnh viện Thẩm mỹ Kangnam ",
   "address": "84 A Bà Huyện Thanh Quan Phường 9 Quận 3",
   "Longtitude": 10.7817505,
   "Latitude": 106.6819346
 },
 {
   "STT": 105,
   "Name": "Bệnh viện Đức Khang",
   "address": "129A Nguyễn Chí Thanh, Phường 9, Quận 5",
   "Longtitude": 10.759761,
   "Latitude": 106.668245
 },
 {
   "STT": 106,
   "Name": "Bệnh viện Mắt Cao Thắng",
   "address": "135B Trần Bình Trọng Phường 12 Quận 5",
   "Longtitude": 10.7817505,
   "Latitude": 106.6819346
 },
 {
   "STT": 107,
   "Name": "Bệnh viện Việt Pháp ( FV. Hospital )",
   "address": "6 Nguyễn Lương Bằng Phường Tân Phú Quận 7",
   "Longtitude": 10.7325029,
   "Latitude": 106.7180068
 },
 {
   "STT": 108,
   "Name": "Bệnh viện Tim Tâm Đức",
   "address": "4 Nguyễn Lương Bằng Phường Tân Phú Quận 7",
   "Longtitude": 10.7335631,
   "Latitude": 106.7178283
 },
 {
   "STT": 109,
   "Name": "Bệnh viện Đa khoa Tân Hưng",
   "address": "871 Trần Xuân Soạn  Phường Tân Hưng Quận 7",
   "Longtitude": 10.7511488,
   "Latitude": 106.6964048
 },
 {
   "STT": 110,
   "Name": "Bệnh viện Phẫu thuật Tạo hình thẩm mỹ Hiệp Lợi",
   "address": "128-130 Dương Bá Trạc Phường 2 Quận 8",
   "Longtitude": 10.7480239,
   "Latitude": 106.6884649
 },
 {
   "STT": 111,
   "Name": "Khu Điều trị Phong Bến Sắn",
   "address": "Ấp Long Bình, Xã Khánh Bình, Huyện Tân Uyên,Hồ Chí Minh",
   "Longtitude": 11.048164,
   "Latitude": 106.742178
 },
 {
   "STT": 112,
   "Name": "Viện Tim",
   "address": "520 Nguyễn Tri Phương,Phường 12,Quận 10,Hồ Chí Minh",
   "Longtitude": 10.773311,
   "Latitude": 106.666931
 },
 {
   "STT": 113,
   "Name": "Viện Y dược học Dân tộc",
   "address": "273 Nguyễn Văn Trỗi,Phường 10,Quận Phú Nhuận,Hồ Chí Minh",
   "Longtitude": 10.797504,
   "Latitude": 106.671343
 },
 {
   "STT": 114,
   "Name": "Trung tâm Y tế dự phòng thành phố Hồ Chí Minh",
   "address": "699 Trần Hưng Đạo,Phường 1,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.755721,
   "Latitude": 106.683508
 },
 {
   "STT": 115,
   "Name": "Trung tâm Y tế dự phòng Quận 1 ",
   "address": "48-52 Mã Lộ,Phường Tân Định,Quận 1,Hồ Chí Minh",
   "Longtitude": 10.793208,
   "Latitude": 106.689443
 },
 {
   "STT": 116,
   "Name": "Trung tâm Y tế dự phòng Quận 2",
   "address": "06 Trịnh Khắc Lập,Phường Thạnh Mỹ Lợi,Quận 2,Hồ Chí Minh",
   "Longtitude": 10.783028,
   "Latitude": 106.757295
 },
 {
   "STT": 117,
   "Name": "Trung tâm Y tế dự phòng Quận 3",
   "address": "80/5 Bà Huyện Thanh Quan,Phường 9,Quận 3,Hồ Chí Minh",
   "Longtitude": 10.782067,
   "Latitude": 106.682289
 },
 {
   "STT": 118,
   "Name": "Trung tâm Y tế dự phòng Quận 4",
   "address": "2 Lê Quốc Hưng,Phường 12,Quận 4,Hồ Chí Minh",
   "Longtitude": 10.765389,
   "Latitude": 106.702652
 },
 {
   "STT": 119,
   "Name": "Trung tâm Y tế dự phòng Quận 5",
   "address": "591A Nguyễn Trãi,Phường 7,Quận 5,Hồ Chí Minh",
   "Longtitude": 10.753797,
   "Latitude": 106.665895
 },
 {
   "STT": 120,
   "Name": "Trung tâm Y tế dự phòng Quận 6",
   "address": "A14/1 Bà Hom,Phường 13,Quận 6,Hồ Chí Minh",
   "Longtitude": 10.754273,
   "Latitude": 106.629441
 },
 {
   "STT": 121,
   "Name": "Trung tâm Y tế dự phòng Quận 7",
   "address": "101 Nguyễn Thị Thập,Phường Tân Phú,Quận 7,Hồ Chí Minh",
   "Longtitude": 10.73742,
   "Latitude": 106.724096
 },
 {
   "STT": 122,
   "Name": "Trung tâm Y tế dự phòng Quận 8",
   "address": "170 Tùng Thiện Vương,Phường 11,Quận 8,Hồ Chí Minh",
   "Longtitude": 10.747822,
   "Latitude": 106.663392
 },
 {
   "STT": 123,
   "Name": "Trung tâm Y tế dự phòng Quận 9",
   "address": "48A đường Tăng Nhơn Phú,Phường Tăng Nhơn Phú B,Quận 9,Hồ Chí Minh",
   "Longtitude": 10.833476,
   "Latitude": 106.779904
 },
 {
   "STT": 124,
   "Name": "Trung tâm Y tế dự phòng Quận 10",
   "address": "475A Cách Mạng Tháng 8,Phường 13,Quận 10,Hồ Chí Minh",
   "Longtitude": 10.782002,
   "Latitude": 106.673632
 },
 {
   "STT": 125,
   "Name": "Trung tâm Y tế dự phòng Quận 11",
   "address": "75 đường số 5 Cư xá Bình Thới,Phường 8,Quận 11,Hồ Chí Minh",
   "Longtitude": 10.760788,
   "Latitude": 106.648355
 },
 {
   "STT": 126,
   "Name": "Trung tâm Y tế dự phòng Quận 12",
   "address": "108 đường HT16,Phường Hiệp Thành,Quận 12,Hồ Chí Minh",
   "Longtitude": 10.874328,
   "Latitude": 106.639205
 },
 {
   "STT": 127,
   "Name": "Trung tâm Y tế dự phòng huyện Bình Chánh ",
   "address": "B7/4A Hương Lộ 11Ấp 2,Xã Tân Quý Tây,Huyện Bình Chánh,Hồ Chí Minh",
   "Longtitude": 10.657582,
   "Latitude": 106.588566
 },
 {
   "STT": 128,
   "Name": "Trung tâm Y tế dự phòng Quận Bình Tân ",
   "address": "549 Hồ Học LãmKhu phố 1,Phường An Lạc,Quận Bình Tân,Hồ Chí Minh",
   "Longtitude": 10.728477,
   "Latitude": 106.606881
 },
 {
   "STT": 129,
   "Name": "Trung tâm Y tế dự phòng Quận Bình Thạnh ",
   "address": "99/6 Nơ Trang Long,Phường 11,Quận Bình Thạnh,Hồ Chí Minh",
   "Longtitude": 10.810229,
   "Latitude": 106.695028
 },
 {
   "STT": 130,
   "Name": "Trung tâm Y tế dự phòng huyện Cần Giờ ",
   "address": "KP Miễu Ba,Thị trấn Cần Thạnh,Huyện Cần Giờ,Hồ Chí Minh",
   "Longtitude": 10.415512,
   "Latitude": 106.973119
 },
 {
   "STT": 131,
   "Name": "Trung tâm Y tế dự phòng huyện Củ Chi",
   "address": "Tỉnh lộ 8 Ấp 12,Xã Tân Thạnh Đông,Huyện Củ Chi,Hồ Chí Minh",
   "Longtitude": 10.983508,
   "Latitude": 106.577627
 },
 {
   "STT": 132,
   "Name": "Trung tâm Y tế dự phòng Quận Gò Vấp",
   "address": "131 Nguyễn Thái Sơn,Phường 7,Quận Gò Vấp,Hồ Chí Minh",
   "Longtitude": 10.824901,
   "Latitude": 106.687094
 },
 {
   "STT": 133,
   "Name": "Trung tâm Y tế dự phòng huyện Hóc Môn",
   "address": "65B KP1 Bà Triệu ,Thị trấn Hóc Môn,Huyện Hóc Môn,Hồ Chí Minh",
   "Longtitude": 10.881564,
   "Latitude": 106.593111
 },
 {
   "STT": 134,
   "Name": "Trung tâm Y tế dự phòng huyện Nhà Bè ",
   "address": "KP4 Huỳnh Tấn Phát,Thị trấn Nhà Bè,Huyện Nhà Bè,Hồ Chí Minh",
   "Longtitude": 10.67674,
   "Latitude": 106.735479
 },
 {
   "STT": 135,
   "Name": "Trung tâm Y tế dự phòng Quận Phú Nhuận",
   "address": "23 Nguyễn Văn Đậu,Phường 5,Quận Phú Nhuận,Hồ Chí Minh",
   "Longtitude": 10.804978,
   "Latitude": 106.686972
 },
 {
   "STT": 136,
   "Name": "Trung tâm Y tế dự phòng Quận Tân Bình ",
   "address": "261 Tân Hải,Phường 13,Quận Tân Bình,Hồ Chí Minh",
   "Longtitude": 10.800491,
   "Latitude": 106.640109
 },
 {
   "STT": 137,
   "Name": "Trung tâm Y tế dự phòng Quận Tân Phú ",
   "address": "44 Thống Nhất,Phường Tân Thành,Quận Tân Phú,Hồ Chí Minh",
   "Longtitude": 10.791333,
   "Latitude": 106.631323
 },
 {
   "STT": 138,
   "Name": "Trung tâm Y tế dự phòng Quận Thủ Đức ",
   "address": "2 Nguyễn Văn Lịch,Phường Linh Tây,Quận Thủ Đức,Hồ Chí Minh",
   "Longtitude": 10.853836,
   "Latitude": 106.752949
 }
];